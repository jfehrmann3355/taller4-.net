﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class AUTOSController : Controller
    {
        private testEntities db = new testEntities();

        // GET: AUTOS
        public ActionResult Index()
        {
            var aUTO = db.AUTO.Include(a => a.MODELO);

            ViewBag.ID_MARCA = new SelectList(db.MARCA, "ID_MARCA", "DESCRIPCION");
            ViewBag.ANO = new SelectList(db.AUTO, "ANO", "ANO");
            return View(aUTO.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int? ID_MARCA, int? ANO, string PATENTE){
            var aUTO = db.AUTO.Include(a => a.MODELO);

            ViewBag.ID_MARCA = new SelectList(db.MARCA, "ID_MARCA", "DESCRIPCION");
            ViewBag.ANO = new SelectList(db.AUTO, "ANO", "ANO");


            if (ID_MARCA == null && string.IsNullOrEmpty(PATENTE) && ANO == null)
            {
                ViewBag.NullVar = "Debe rellenar un campo al menos";
                return View(aUTO);
            }
            else{ 
                if (string.IsNullOrEmpty(PATENTE) && ANO == null){
                    return View(aUTO.Where(x => x.MODELO.MARCA.ID_MARCA == ID_MARCA).ToList());
                }
                if (string.IsNullOrEmpty(PATENTE) && ID_MARCA == null){
                    return View(aUTO.Where(x => x.ANO == ANO).ToList());
                }
                if (ANO == null && ID_MARCA == null){
                    return View(aUTO.Where(x => x.PATENTE == PATENTE).ToList());
                }
                if (ANO == null) {
                    return View(aUTO.Where(x => x.MODELO.MARCA.ID_MARCA == ID_MARCA && x.PATENTE == PATENTE).ToList());
                }
                if (ID_MARCA == null){
                    return View(aUTO.Where(x => x.PATENTE == PATENTE && x.ANO == ANO).ToList());
                }
                if (string.IsNullOrEmpty(PATENTE)){
                    return View(aUTO.Where(x => x.MODELO.MARCA.ID_MARCA == ID_MARCA && x.ANO == ANO).ToList());
                }
                return View(aUTO.Where(x => x.MODELO.MARCA.ID_MARCA == ID_MARCA && x.PATENTE == PATENTE && x.ANO == ANO).ToList());
            }
        }

        // GET: AUTOS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTO aUTO = db.AUTO.Find(id);
            if (aUTO == null)
            {
                return HttpNotFound();
            }
            return View(aUTO);
        }

        // GET: AUTOS/Create
        public ActionResult Create()
        {
            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO");
            return View();
        }

        // POST: AUTOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID_AUTO,ID_MODELO,PATENTE,ANO,COLOR,OBSERVACION")] AUTO aUTO)
        {
            if (ModelState.IsValid){
                string patente = aUTO.PATENTE;
                var patente_validar = db.AUTO.Where(x => x.PATENTE.Equals(patente)).FirstOrDefault();
                //Aqui se verifica si la patente ya esta repetida
                if (patente_validar != null){
                    ViewBag.MessageDuplicate = "Error, la patente ya se encuentra registrada.";
                }
                else if (aUTO.ANO > 2022){
                    ViewBag.MessageInvalid = "No se permite registrar autos con mas de 2 años de vigencia.";
                }
                else{
                    if (aUTO.ANO < 1990){
                        aUTO.OBSERVACION = "ANTIGUO";
                        db.AUTO.Add(aUTO);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else{
                        aUTO.OBSERVACION = "SIN OBSERVACIONES";
                        db.AUTO.Add(aUTO);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }

                }

            }

            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO", aUTO.ID_MODELO);
            return View(aUTO);
        }

        // GET: AUTOS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTO aUTO = db.AUTO.Find(id);
            if (aUTO == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO", aUTO.ID_MODELO);
            return View(aUTO);

        }

        // POST: AUTOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_AUTO,ID_MODELO,PATENTE,ANO,COLOR,OBSERVACION")] AUTO aUTO)
        {
            if (ModelState.IsValid){
                string patente = aUTO.PATENTE;
                var patente_validar = db.AUTO.Where(x => x.PATENTE.Equals(patente)).FirstOrDefault();
                if (patente_validar != null){
                    ViewBag.MessageDuplicate = "Error, la patente ya se encuentra registrada.";
                }
                else if (aUTO.ANO > 2022){
                    ViewBag.MessageInvalid = "No se permite registrar autos con mas de 2 años de vigencia.";
                }
                else{
                    if (aUTO.ANO < 1990){
                        aUTO.OBSERVACION = "ANTIGUO";
                        db.Entry(aUTO).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    else{
                        aUTO.OBSERVACION = "SIN OBSERVACIONES";
                        db.Entry(aUTO).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
            }
            ViewBag.ID_MODELO = new SelectList(db.MODELO, "ID_MODELO", "DESCRIPCION_MODELO", aUTO.ID_MODELO);
            return View(aUTO);
        }

        // GET: AUTOS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AUTO aUTO = db.AUTO.Find(id);
            if (aUTO == null)
            {
                return HttpNotFound();
            }
            return View(aUTO);
        }

        // POST: AUTOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AUTO aUTO = db.AUTO.Find(id);
            db.AUTO.Remove(aUTO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
