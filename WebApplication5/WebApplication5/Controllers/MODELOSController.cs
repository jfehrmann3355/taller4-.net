﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class MODELOSController : Controller
    {
        private testEntities db = new testEntities(); // vinculo a la base de datos.

        // GET: MODELOS
        public ActionResult Index()
        {
            var mODELO = db.MODELO.Include(m => m.MARCA); //var= soporta cualquier tipo de dato.
                                                          //muestra la marca 

            //var mODELO = db.MODELO.Where(x => x.ID_MARCA==1).Include(m => m.MARCA);
            //Mostrar todos los modelos según una condición.
            return View(mODELO.ToList()); //para mostrar varios datos.
        }

        // GET: MODELOS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MODELO mODELO = db.MODELO.Find(id);
            if (mODELO == null)
            {
                return HttpNotFound();
            }
            return View(mODELO);
        }

        // GET: MODELOS/Create
        public ActionResult Create()
        {
            ViewBag.ID_MARCA = new SelectList(db.MARCA, "ID_MARCA", "DESCRIPCION");
            //create un select con los datos necesarios.
            return View();
        }

        // POST: MODELOS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        //bind= se crea una instancia y se rellena en los atributos de la tabla modelo. 
        public ActionResult Create([Bind(Include = "ID_MODELO,ID_MARCA,DESCRIPCION_MODELO")] MODELO mODELO)
        {
            if (ModelState.IsValid) //validaciones aqui.
            {
                mODELO.DESCRIPCION_MODELO = mODELO.DESCRIPCION_MODELO.ToUpper();
                db.MODELO.Add(mODELO);
                /*insert */
                db.SaveChanges();
                /* reedireccion */
                return RedirectToAction("Index");
            }

            ViewBag.ID_MARCA = new SelectList(db.MARCA, "ID_MARCA", "DESCRIPCION", mODELO.ID_MARCA);
            return View(mODELO);
        }

        // GET: MODELOS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MODELO mODELO = db.MODELO.Find(id); //se busca el modelo asociada a la id.
            if (mODELO == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID_MARCA = new SelectList(db.MARCA, "ID_MARCA", "DESCRIPCION", mODELO.ID_MARCA);
            return View(mODELO);
        }

        // POST: MODELOS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID_MODELO,ID_MARCA,DESCRIPCION_MODELO")] MODELO mODELO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mODELO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ID_MARCA = new SelectList(db.MARCA, "ID_MARCA", "DESCRIPCION", mODELO.ID_MARCA);
            return View(mODELO);
        }

        // GET: MODELOS/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MODELO mODELO = db.MODELO.Find(id);
            if (mODELO == null)
            {
                return HttpNotFound();
            }
            return View(mODELO);
        }

        // POST: MODELOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            MODELO mODELO = db.MODELO.Find(id);
            db.MODELO.Remove(mODELO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
